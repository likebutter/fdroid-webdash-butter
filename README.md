<!--
SPDX-FileCopyrightText: 2023 Michael Pöhn <michael@poehn.at>
SPDX-License-Identifier: GPL-3.0-or-later
-->

# fdroid-webdash

web-app for browsing F-Droid repositories.

## screenshots

<img height="150" src="metadata/en-US/images/phoneScreenshots/screenshot1.png">
<img height="150" src="metadata/en-US/images/phoneScreenshots/screenshot2.png">
<img height="150" src="metadata/en-US/images/phoneScreenshots/screenshot3.png">
<img height="150" src="metadata/en-US/images/phoneScreenshots/screenshot4.png">
<img height="150" src="metadata/en-US/images/phoneScreenshots/screenshot5.png">
<img height="150" src="metadata/en-US/images/phoneScreenshots/screenshot6.png">

## web deployment hints

⚠️ Privacy Note: This app is not GDPR compliant, run at own risk. (We've
observed Flutter apps leaking leak user data to fonts.google.com and unpkg.com)

### deployment

To deploy this app, you need to build it and extract all files in `build/web`
from your project colder to your web server. It will try to guess the correct
path of your index-v2.json file based on browsers current location. There are 3
places relative to your /fdroid/repo folder where you can deploy the web app:

* `/fdroid/repo/index.html` - will try to access `./index-v2.json`
* `/fdroid/index.html` - will try to access: `./repo/index-v2.json`
* `/index.html` - will try to access: `./fdroid/repo/index-v2.json`

(`index.html` is the entry point of this web application.)

### Content securit policiy

Since this app is build using Flutter SDK it will need rather weak CSP.

```
script-src: 'wasm-unsafe-eval'; media-src data:; connect-src https://fonts.gstatic.com/;
```

(this might not be complete or might need some tweaking.)

### do a web release build

```
flutter build web --dart-define=FLUTTER_WEB_CANVASKIT_URL=/canvaskit/ --pwa-strategy offline-first --base-href=/ --csp
```

The release artefact will show up in `build/web`.

If you're planning hosting this PWA in a sub-directory you'll need to bake the
path into the app. e.g. if you're going to deploy to
__https://example.com/fdroid/browse__ you'll need above command to:

```
flutter build web --dart-define=FLUTTER_WEB_CANVASKIT_URL=/fdroid/browse/canvaskit/ --pwa-strategy offline-first --base-href=/fdroid/browse/ --csp
```

## development hints

### start a local dev server

Note: To get around CORS for accessing index files of remote F-Droid repos you might
want to use this handy tool before trying to run a dev server:
https://pub.dev/packages/flutter_cors

You can either open the project in Android Studio (with flutter plugin) or
start a development server in a terminal:

```
flutter run -d chrome --web-renderer html
```

(Using `-d web-server` will just start a dev webserver without launching chrome
browser, although in this case you'll have to take care of CORS some other
way.)

