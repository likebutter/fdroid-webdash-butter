// SPDX-FileCopyrightText: 2023 Michael Pöhn <michael@poehn.at>
// SPDX-License-Identifier: GPL-3.0-or-later

import 'package:fdroidwebdash/screens/add_repo_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';

import './providers/flavor_state.dart';
import './providers/packages_state.dart';
import './providers/settings_state.dart';
import './screens/landing_screen.dart';
import './screens/package_details_screen.dart';
import './screens/packages_list_screen.dart';
import './widgets/loading_widget.dart';

class WebDashApp extends StatelessWidget {
  const WebDashApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
            create: (BuildContext context) => FlavorSettings.defaults(context)),
        ChangeNotifierProvider(
            create: (BuildContext context) => SettingsState.defaults(context)),
        FutureProvider<PackagesState>(
          initialData: PackagesState.empty(),
          create: (BuildContext context) => PackagesState.fetch(context),
          catchError: (context, error) {
            return PackagesState.empty();
          },
        ),
      ],
      child: const MaterialUI(),
    );
  }
}

class MaterialUI extends StatelessWidget {
  const MaterialUI({super.key});

  @override
  Widget build(BuildContext context) {
    var flavor = Provider.of<FlavorSettings>(context);
    var packagesState = Provider.of<PackagesState>(context);
    var initialLoc = "/packages";

    String title =
        "F-Droid"; // Translations yet initalized, so not translating this here.
    if (flavor.isPwa()) {
      initialLoc = "/";
      if (packagesState.repos.length == 1) {
        title = packagesState.repos.values.first.name.get() ?? title;
      }
    }

    final routes = [
      GoRoute(
        path: '/',
        builder: (context, state) => const LandingScreen(),
        routes: [
          GoRoute(
              path: 'add_repo',
              builder: (context, state) => const AddRepoScreen()),
        ],
      ),
      GoRoute(
        path: '/packages',
        builder: (context, state) => const PackagesScreen(),
        routes: [
          GoRoute(
            path: ':packageName',
            builder: (context, state) {
              return PackageDetailsScreen(
                packageName: state.pathParameters['packageName']!,
              );
            },
          ),
        ],
      ),
    ];

    return MaterialApp.router(
      title: title,
      theme: ThemeData(
        useMaterial3: true,
        fontFamily: "roboto-offline",
        colorScheme: ColorScheme.fromSeed(
          brightness: Brightness.light,
          seedColor: Colors.grey,
          primary: Colors.lightBlueAccent,
          surface: const Color(0xffe6f1f2),
          secondaryContainer: const Color(0xfface7ec),
        ),
      ),
      darkTheme: ThemeData(
        useMaterial3: true,
        fontFamily: "roboto-offline",
        colorScheme: ColorScheme.fromSeed(
          brightness: Brightness.dark,
          seedColor: Colors.grey,
          surface: const Color(0xff3d4a4f),
          secondaryContainer: const Color(0xff64ffda),
          onSecondaryContainer: const Color(0xff000000),
        ),
      ),
      themeMode: ThemeMode.system,
      routerConfig: GoRouter(
        initialLocation: initialLoc,
        routes: [
          ShellRoute(
            builder: (context, state, child) {
              return ScaffoldWrapper(child: child);
            },
            routes: routes,
          ),
        ],
      ),
      localizationsDelegates: const [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale("en"),  // primary language
        Locale("de"),
      ],
    );
  }
}

class ScaffoldWrapper extends StatelessWidget {
  final Widget child;

  const ScaffoldWrapper({super.key, required this.child});

  @override
  Widget build(BuildContext context) {
    List<NavDef> navDefs = [
      NavDef(
        label: AppLocalizations.of(context)!.navRepo,
        icon: Icons.home_filled,
        routeMatcher: '^\\/\$',
        onTap: () {
          GoRouter.of(context).go("/");
        },
      ),
      NavDef(
        label: AppLocalizations.of(context)!.navApps,
        icon: Icons.apps,
        routeMatcher: '^\\/packages\$',
        onTap: () {
          GoRouter.of(context).go('/packages');
        },
      ),
    ];

    return Loading(
      child: LayoutBuilder(
        builder: (context, constraints) {
          final selectedIndex = _findSelected(navDefs, context);

          if (selectedIndex < 0) {
            return Scaffold(
              backgroundColor: Theme.of(context).scaffoldBackgroundColor,
              body: child,
            );
          }

          if (constraints.maxWidth >= 650) {
            return Scaffold(
              backgroundColor: Theme.of(context).scaffoldBackgroundColor,
              body: Row(
                children: [
                  NavigationRail(
                    destinations: List.of(
                        navDefs.map((e) => e.buildNavigationRailDestination())),
                    selectedIndex: selectedIndex,
                    onDestinationSelected: (int index) {
                      navDefs[index].onTap();
                    },
                  ),
                  Expanded(child: child),
                ],
              ),
            );
          } else {
            return Scaffold(
              backgroundColor: Theme.of(context).scaffoldBackgroundColor,
              body: SafeArea(
                child: child,
              ),
              bottomNavigationBar: NavigationBar(
                destinations:
                    List.of(navDefs.map((e) => e.buildNavigationDestination())),
                selectedIndex: selectedIndex,
                onDestinationSelected: (int index) {
                  navDefs[index].onTap();
                },
              ),
            );
          }
        },
      ),
    );
  }
}

class NavDef {
  final String label;
  final IconData icon;
  final String routeMatcher;
  final Function onTap;

  NavDef(
      {required this.label,
      required this.icon,
      required this.routeMatcher,
      required this.onTap});

  NavigationDestination buildNavigationDestination() {
    return NavigationDestination(
      icon: Icon(icon),
      label: label,
    );
  }

  NavigationRailDestination buildNavigationRailDestination() {
    return NavigationRailDestination(
      icon: Icon(icon),
      label: Text(label),
    );
  }

  bool isSelected(BuildContext context, location) {
    return RegExp(routeMatcher).hasMatch(location);
  }
}

int _findSelected(List<NavDef> navDefs, BuildContext context) {
  final String location = GoRouter.of(context).location;
  for (int i = 0; i < navDefs.length; i++) {
    if (navDefs[i].isSelected(context, location)) {
      return i;
    }
  }
  return -1;
}
