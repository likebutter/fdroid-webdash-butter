// SPDX-FileCopyrightText: 2023 Michael Pöhn <michael@poehn.at>
// SPDX-License-Identifier: GPL-3.0-or-later

T? localize<T>(Map<String, T>? localizedMap) {
  // right new we don't to any actual localization, instead we just take
  // whatever is available with a preference for english
  if (localizedMap == null || localizedMap.isEmpty) {
    return null;
  } else if (localizedMap.containsKey('en-US')) {
    return localizedMap['en-US'];
  } else if (localizedMap.containsKey('en')) {
    return localizedMap['en'];
  } else {
    return localizedMap.values.elementAt(0);
  }
}

String stripHtmlTags(String? text) {
  return text == null ? '' : text.replaceAll(RegExp(r'<[^>]*>|&[^;]+;'), ' ');
}