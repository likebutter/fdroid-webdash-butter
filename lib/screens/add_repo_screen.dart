// SPDX-FileCopyrightText: 2023 Michael Pöhn <michael@poehn.at>
// SPDX-License-Identifier: GPL-3.0-or-later

import 'package:fdroidwebdash/providers/flavor_state.dart';
import 'package:fdroidwebdash/providers/packages_state.dart';
import 'package:fdroidwebdash/widgets/responsive_padding.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';

import '../api/index_v2_api.dart';
import '../utils/platform_specific.dart';

class AddRepoScreen extends StatelessWidget {
  const AddRepoScreen({super.key});

  @override
  Widget build(BuildContext context) {
    FlavorSettings flavorSettings = Provider.of<FlavorSettings>(context);
    PackagesState packagesState = Provider.of<PackagesState>(context);
    Repo? repo = packagesState.repos.values.first;

    var repolink = "https://fdroid.link/#${repo.address}";

    return Container(
      color: Theme.of(context).colorScheme.background,
      child: CustomScrollView(
        slivers: [
          SliverAppBar(
            title: Text(AppLocalizations.of(context)!.addRepository),
            leading: const BackButton(),
          ),
          SliverToBoxAdapter(
            child: ResponsivePadding(
              child: Column(
                children: [
                  const SizedBox(height: 32),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      const SizedBox(width: 16),
                      Expanded(
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(32),
                          child: Material(
                            child: InkWell(
                              child: Column(
                                children: [
                                  const SizedBox(height: 32),
                                  QrImageView(
                                    data: repolink,
                                    version: QrVersions.auto,
                                    size: 200.0,
                                    foregroundColor: Theme.of(context)
                                        .colorScheme
                                        .onBackground,
                                    backgroundColor: Theme.of(context)
                                        .colorScheme
                                        .background,
                                    eyeStyle: QrEyeStyle(
                                      color: Theme.of(context)
                                          .colorScheme
                                          .onBackground,
                                      eyeShape: QrEyeShape.square,
                                    ),
                                  ),
                                  const SizedBox(height: 8),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Text(
                                        repolink,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      const SizedBox(width: 4),
                                      const Icon(Icons.copy, size: 18),
                                    ],
                                  ),
                                  const SizedBox(height: 32),
                                ],
                              ),
                              onTap: () {
                                Clipboard.setData(
                                  ClipboardData(
                                    text: repolink,
                                  ),
                                );
                                ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    content: Text(AppLocalizations.of(context)!
                                        .copiedToClipboard),
                                  ),
                                );
                              },
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(width: 16),
                    ],
                  ),
                  const SizedBox(height: 32),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      FittedBox(
                        child: FilledButton(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              const Icon(Icons.install_mobile_outlined,
                                  size: 18),
                              const SizedBox(width: 4),
                              Text(AppLocalizations.of(context)!.repoLink)
                            ],
                          ),
                          onPressed: () {
                            if (flavorSettings.isPwa()) {
                              var url = Uri.parse(repo.address.toString());
                              platformSpecificUtils().openUrl(
                                  fileUrl: url
                                      .replace(scheme: "fdroidrepo")
                                      .toString());
                            }
                          },
                        ),
                      ),
                      const SizedBox(width: 16),
                      FittedBox(
                        child: FilledButton(
                          child: Row(children: [
                            const Icon(Icons.link, size: 18),
                            const SizedBox(width: 4),
                            Text(AppLocalizations.of(context)!.webLink)
                          ]),
                          onPressed: () {
                            if (flavorSettings.isPwa()) {
                              var url = Uri.parse(repo.address.toString());
                              platformSpecificUtils().openUrl(
                                  fileUrl:
                                      url.replace(scheme: "https").toString());
                            }
                          },
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
