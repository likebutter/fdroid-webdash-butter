// SPDX-FileCopyrightText: 2023 Michael Pöhn <michael@poehn.at>
// SPDX-License-Identifier: GPL-3.0-or-later

import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

import '../api/index_v2_api.dart';
import '../providers/flavor_state.dart';
import '../providers/packages_state.dart';
import '../utils/platform_specific.dart';
import '../utils/text_helper.dart';
import '../widgets/app_icon.dart';
import '../widgets/generic_feature_graphic.dart';
import '../widgets/responsive_padding.dart';

class PackageDetailsScreen extends StatelessWidget {
  final String packageName;

  const PackageDetailsScreen({super.key, required this.packageName});

  @override
  Widget build(BuildContext context) {
    var packagesState = Provider.of<PackagesState>(context);
    Package? package;
    if (packagesState.packages.isNotEmpty) {
      package = packagesState.packages[packageName];
    }

    return CustomScrollView(
      slivers: [
        SliverAppBar(
          pinned: true,
          leading: const BackButton(),
          title: Text(
            package?.metadata.name.get() ??
                AppLocalizations.of(context)!.unnamedApp,
          ),
          backgroundColor: Theme.of(context).canvasColor,
        ),
        SliverToBoxAdapter(
          child: ResponsivePadding(
            child: Column(
              children: [
                const SizedBox(height: 16),
                TitleBlock(
                  package: package,
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 32, horizontal: 32),
                  child: ExpandableDescriptionBlock(
                    description:
                        stripHtmlTags(package?.metadata.description.get()),
                    whatsNew: stripHtmlTags(
                        package?.versions.entries.first.value.whatsNew.get()),
                    versionName: package
                        ?.versions.entries.first.value.manifest.versionName,
                  ),
                ),
                ScreenshotBlock(
                  package: package,
                ),
                const SizedBox(
                  height: 64,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class ExpandableDescriptionBlock extends StatefulWidget {
  final String description;
  final String whatsNew;
  final String? versionName;

  const ExpandableDescriptionBlock(
      {super.key,
      required this.description,
      required this.whatsNew,
      required this.versionName});

  @override
  State<ExpandableDescriptionBlock> createState() =>
      _ExpandableDescriptionBlockState();
}

class _ExpandableDescriptionBlockState
    extends State<ExpandableDescriptionBlock> {
  bool expanded = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        expanded
            ? Text(stripHtmlTags(widget.description))
            : Text(
                stripHtmlTags(widget.description),
                maxLines: 5,
                overflow: TextOverflow.ellipsis,
              ),
        expanded && widget.whatsNew.isNotEmpty
            ? Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 16,
                  ),
                  Text(
                      AppLocalizations.of(context)!.newIn(widget.versionName!)),
                  const SizedBox(
                    height: 8,
                  ),
                  Text(stripHtmlTags(widget.whatsNew)),
                ],
              )
            : const SizedBox(
                height: 0,
                width: 0,
              ),
        const SizedBox(
          height: 16,
        ),
        Center(
          child: MaterialButton(
            child: Text(expanded
                ? AppLocalizations.of(context)!.showLess.toUpperCase()
                : AppLocalizations.of(context)!.showMore.toUpperCase()),
            onPressed: () {
              setState(() {
                expanded = !expanded;
              });
            },
          ),
        ),
      ],
    );
  }
}

class ScreenshotBlock extends StatelessWidget {
  final Package? package;

  const ScreenshotBlock({super.key, required this.package});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 32 - 4),
        child: Row(
          children: package?.metadata.screenshots?.phone == null
              ? []
              : List<Widget>.from(package!.metadata.screenshots!.phone!.iconUrls
                  .map((e) => Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 4),
                        child: Material(
                          elevation: 2,
                          borderRadius: BorderRadius.circular(8),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(8),
                            child: Image.network(
                              '${package?.repoUrl}$e',
                              height: 250,
                            ),
                          ),
                        ),
                      ))),
        ),
      ),
    );
  }
}

class TitleBlock extends StatelessWidget {
  final Package? package;

  const TitleBlock({super.key, required this.package});

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            width: 32,
          ),
          Expanded(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(32),
              child: Container(
                color: Theme.of(context).colorScheme.surface,
                child: Column(
                  children: [
                    FeatureGraphic(package: package!),
                    TitleIconTextBlock(
                      package: package,
                    ),
                  ],
                ),
              ),
            ),
          ),
          const SizedBox(
            width: 32,
          ),
        ],
      ),
    ]);
  }
}

class TitleIconTextBlock extends StatelessWidget {
  final Package? package;

  const TitleIconTextBlock({super.key, required this.package});

  @override
  Widget build(BuildContext context) {
    FlavorSettings flavor = Provider.of<FlavorSettings>(context);

    return Padding(
      padding: const EdgeInsets.all(12 + 16),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AppIcon(package: package),
              const SizedBox(
                width: 12,
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      package?.metadata.name.get() ??
                          AppLocalizations.of(context)!.unnamedApp,
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    Text(
                      package?.metadata.summary.get() ?? '',
                    ),
                  ],
                ),
              ),
            ],
          ),
          if (flavor.isPwa())
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 24, 0, 0),
                  child: WebButtons(
                    package: package!,
                  ),
                ),
              ],
            )
        ],
      ),
    );
  }
}

class WebButtons extends StatelessWidget {
  final Package package;

  const WebButtons({super.key, required this.package});

  @override
  Widget build(BuildContext context) {
    var dlText = AppLocalizations.of(context)!.download;
    List<String> knownExtensions = ["APK", "IPA", "PDF", "ZIP"];
    var fileName = package.versions.entries.first.value.file.name;
    var fileExtension =
        fileName.substring(fileName.length - 3, fileName.length).toUpperCase();
    if (knownExtensions.contains(fileExtension)) {
      dlText = AppLocalizations.of(context)!.downloadFile(fileExtension);
    }

    return FilledButton(
      onPressed: () async {
        var url = Uri.parse(
            '${package.repoUrl}${package.versions.values.first.file.name}');
        platformSpecificUtils().downloadFile(fileUrl: url.toString());
      },
      child: Row(
        children: [
          const Icon(
            Icons.download,
            size: 18,
          ),
          const SizedBox(
            width: 2,
          ),
          Text(dlText),
          const SizedBox(
            width: 2,
          ),
        ],
      ),
    );
  }
}
