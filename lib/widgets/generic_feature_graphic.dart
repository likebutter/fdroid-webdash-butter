// SPDX-FileCopyrightText: 2023 Michael Pöhn <michael@poehn.at>
// SPDX-License-Identifier: GPL-3.0-or-later

import 'package:fdroidwebdash/api/index_v2_api.dart';
import 'package:flutter/material.dart';

class FeatureGraphic extends StatelessWidget {
  final Package package;

  const FeatureGraphic({super.key, required this.package});

  @override
  Widget build(BuildContext context) {
    return package.metadata.featureGraphic?.iconUrl == null
        ? GenericFeatureGraphic(
            patternSeed: package.packageName.hashCode,
          )
        : Image.network(
            '${package.repoUrl}/${package.metadata.featureGraphic!.iconUrl!}',
            fit: BoxFit.cover,
          );
  }
}

class GenericFeatureGraphic extends StatelessWidget {
  final int patternSeed;

  const GenericFeatureGraphic({super.key, required this.patternSeed});

  @override
  Widget build(BuildContext context) {
    return CustomPaint(
      painter: FeatureGraphicPainter(seed: patternSeed),
      size: const Size.fromHeight(180),
    );
  }
}

class FeatureGraphicPainter extends CustomPainter {
  final int seed;

  FeatureGraphicPainter({required this.seed});

  @override
  void paint(Canvas canvas, Size size) {
    List<Path> tris = [];
    for (int y = 0; y < 2; y++) {
      for (int x = 0; x < 4; x++) {
        if ((x + (y % 2)) % 2 == 0) {
          // +
          // | \
          // + - +
          var p = Path();
          p.moveTo(x * (size.width / 4), y * (size.height / 2));
          p.lineTo(x * (size.width / 4), (y + 1) * (size.height / 2));
          p.lineTo((x + 1) * (size.width / 4), (y + 1) * (size.height / 2));
          p.close();
          tris.add(p);

          // + - +
          //   \ |
          //     +
          p = Path();
          p.moveTo(x * (size.width / 4), y * (size.height / 2));
          p.lineTo((x + 1) * (size.width / 4), y * (size.height / 2));
          p.lineTo((x + 1) * (size.width / 4), (y + 1) * (size.height / 2));
          p.close();
          tris.add(p);
        } else {
          // + - +
          // | /
          // +
          var p = Path();
          p.moveTo(x * (size.width / 4), (y + 1) * (size.height / 2));
          p.lineTo(x * (size.width / 4), y * (size.height / 2));
          p.lineTo((x + 1) * (size.width / 4), y * (size.height / 2));
          p.close();
          tris.add(p);

          //     +
          //   / |
          // + - +
          p = Path();
          p.moveTo((x + 1) * (size.width / 4), y * (size.height / 2));
          p.lineTo((x + 1) * (size.width / 4), (y + 1) * (size.height / 2));
          p.lineTo(x * (size.width / 4), (y + 1) * (size.height / 2));
          p.close();
          tris.add(p);
        }
      }
    }

    canvas.drawRect(Rect.fromLTRB(0, 0, size.width, size.height),
        Paint()..color = const Color(0x33000000));
    final Paint c = Paint()..color = const Color(0x44ffffff);
    for (int i = 0; i < tris.length; i++) {
      if (seed % (i+1) == 0) {
        canvas.drawPath(tris[i], c);
      }
    }
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;
}
