// SPDX-FileCopyrightText: 2023 Michael Pöhn <michael@poehn.at>
// SPDX-License-Identifier: GPL-3.0-or-later

import 'package:flutter/material.dart';
import '../api/index_v2_api.dart';

class AppIcon extends StatelessWidget {
  final Package? package;

  const AppIcon({super.key,  required this.package });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: 46,
        height: 46,
        child: Material(
          borderRadius: BorderRadius.circular(8),
          elevation: 2,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(8),
            child: package?.metadata.icon?.iconUrl == null || package?.repoUrl == null
                ? Image.asset('assets/ic_repo_app_default.png')
                : Image.network('${package!.repoUrl}${package!.metadata.icon!.iconUrl}'),
          ),
        ));
  }
}